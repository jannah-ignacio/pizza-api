const User = require('../models/User');
const Cart = require('../models/Cart');
const Order = require('../models/Order');
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth')

//Check if email already exists
//will be used for validation during user registration
module.exports.checkEmail = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true //Email exists.
		} else {
			return false //No registered email yet.
		}
	})
}

//User Registration
module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then((result) => {
		if (result == null) {
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo
			});
			//console.log(newUser)
			return newUser.save().then((savedUser, error) => {
				if (error) {
					//console.log(error)
					return false
				} else {
					return true //"User is successfully registered."
				}
			})
		
		} else {
			return false //"Email already exists."
		}
	})
};

//User Login/Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => { 
		if(result === null){
			return false //"Email is not found."
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return false //"Email or Password did not match."
			}
		}
		})
};

//Get profile of all users
module.exports.getUser = () => {
	return User.find({}).then(result => {
		result.password = ""
		return result
	})
}	

//Get one user's profile
module.exports.getOneUser = (userId) => {
	return User.findById(userId).then(result => {
		result.password = ""
		return result
	})
}

//Updating of user's profile
module.exports.userUpdate = (data) => {
	
	const updatedProfile = ({	
		firstName: data.updateUser.firstName,
		lastName: data.updateUser.lastName,
		email: data.updateUser.email,
		password: data.hashSync(data.updateUser.password, 10),
		mobileNo: data.updateUser.mobileNo
		})	
	
	return User.findByIdAndUpdate(data.userId, updatedProfile).then((result, error) => {
		if (error) {
			return false //"User data cannot be updated."
		} else {
			return result //`User data is updated successfully.` 
		}
	})
}

module.exports.createCart= async (data)=>{

	let amount = await Product.findById(data.productId).then((product, error) => {
		console.log(product)
		if (error){
			return false
		} else{
			return price = product.price
			}
	})

	let isProductActive = await Product.findById(data.productId).then(result=>{
		return result.isActive
	})

	let productName = await Product.findById(data.productId).then(result=>{
		return result.name
	})
	// console.log(productName)
	// push to user model
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		let test=user.cart.find(id=>id.productId==data.productId)
		// test if user has no orders on that product yet
		// if none push to orders array in User model
		// if yes, increment quantity element in orders array
			if(test==undefined){
				if(isProductActive){
						user.cart.push({
							productId:data.productId,
							name:productName,
							quantity:data.quantity})
				} 
			} else{

				if(isProductActive){

				test.quantity+=data.quantity
				
				} 	
			}
			
			// console.log(user.cart)
			return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return true
					}
				})

	})
	
	// push to Cart Model
	let isCartUpdated=await Cart.findOne({orderedBy: data.userId}).then(cart=>{
		
		let result = cart.products.find(id => id.productId == data.productId)
		
		if(cart == null){
			if(isProductActive){
				let newCart= new Cart({
					
					products:{
						productId:data.productId,
						name: productName,
						quantity:data.quantity,
						subtotal:amount*data.quantity
						
					},
					cartTotal:amount*data.quantity,
					orderedBy:data.userId
				})
			 return newCart.save().then((order,error)=>{
					if (error){
						return false
					} else{
						return true
					}
				})
		} else {
				return
			}
	} else { 
			let test = cart.products.find(id=>id.productId==data.productId)
			
			console.log(test)
			if(test==undefined){
				if(isProductActive){
					cart.products.push({
					productId:data.productId,
					name: productName,
					quantity:data.quantity,
					subtotal:amount*data.quantity	
					})
			} else {
				return
			}
			} else {
				if (isProductActive){
					test.quantity+=data.quantity
					
					test.subtotal=test.quantity*amount
			} else {
				return
			}
			}			
		}
		let sum = 0
			for(let i=0;i<cart.products.length;i++){
					sum+=cart.products[i].subtotal
			}
			cart.cartTotal=sum

		return cart.save().then((order,error)=>{
					if(error){
						return false
					} else {
						return result
					}
				})
		
	})	

		
	return isCartUpdated
	
	
}

// get all orders
module.exports.getAllOrders=()=>{
	return Order.find({}).then(order=>{
		if (order.length>0){
			return order
		} else {
			return false
		}
	})
}


// get specific order
module.exports.getOrder=(data)=>{
return userOrder= Order.find({orderedBy:data.userId}).then(order=>{
	
		if (order==null || order==' ' ||order==undefined){
			return `You don't have any orders yet!`
		} else {
			return order

		}
		
		
		

	})
}

// delete all cart items

module.exports.deleteAllOrder= async (data)=>{
	let cart= await Cart.findOneAndDelete({orderedBy:data.userId}).then(result=>{
		if (result==null || result==' ' ||result==undefined){
			return `You don't have any items yet!`
		} else {
			return true
		}
	})
	// console.log (order)

	let cartUser = await User.findById(data.userId).then(user=>{
		user.cart.splice(0,user.cart.length)
		return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return user
					}
				})
	})
	// console.log(orderUser)

	if(cartUser && cart){
		return `Order has been deleted`
	} else{
		return false
	}
}

// delete product/product quantity in order
module.exports.deleteProduct= async (data)=>{
	let amount= await Product.findById(data.productId).then( (p,error)=>{
			if (error){
				return false
			} else{
				return price=p.price
			}

			})
	let isUserUpdated= await User.findById(data.userId).then(user=>{
		let test=user.cart.find(id=>id.productId==data.productId)
		let index = user.cart.findIndex(i=>i.productId==data.productId)
		
		
			if(test==undefined){
				return false
			} else{
				test.quantity--
				if (test.quantity <= 0){
					user.cart.splice(index, 1)
				} 
				return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return test
					}
				})
			}		
	})
	// console.log(isUserUpdated)
	let isCartUpdated= await Cart.findOne({orderedBy:data.userId}).then(cart=>{
		let test=cart.products.find(id=>id.productId==data.productId)
		let index = cart.products.findIndex(i=>i.productId==data.productId)
		// console.log(order)
			if (test===undefined){
				return false
			} else {
				test.quantity--
				test.subtotal=test.quantity*amount
				if (test.quantity<=0){
					cart.products.splice(index,1)
				} 
				
				return cart.save().then((order,error)=>{
					if(error){
						return false
					} else {
						// let product=order.products.find(id=>id.productId==data.productId)
						return test
					}
				})
			}
	})
	let cartTotal=await Cart.findOne({orderedBy:data.userId}).then(cart=>{
		// console.log(cart)
		// console.log(cart.length)
		let sum = 0
			for(let i=0;i<cart.products.length;i++){
					sum+=cart.products[i].subtotal
			}
			cart.cartTotal=sum
		// console.log(sum)
		cart.save()

			/*let sum=0
			for(a=0;a<subtotals.length;a++){
				console.log(subtotals[a].subtotal)
				sum += subtotals[a].subtotal
				
			cart.cartTotal=sum

			// console.log(order[i].totalAmount)
			}
			// console.log(sum)
			// cart[i].save()
		
		return cart[0]*/
	})
	// console.log(isOrderUpdated)		
		
				return isCartUpdated
			
}

// get cart
module.exports.getCart=(data)=>{
	return userCart=Cart.findOne({orderedBy:data.userId}).then(cart=>{
		
			if (cart==null || cart==' ' ||cart==undefined){
				return `You don't have any orders yet!`
			} else {
				return cart
			}
			

		})
		

}

// create order
module.exports.createOrder= async (data) => {

	let orderItems = await Cart.findOne({orderedBy:data.userId}).exec()

	let order = await Order.findOne({orderedBy:data.userId}).then(order => {
		if(orderItems.products.length > 0){
		let newOrder= new Order({
					
					products: orderItems.products,
					totalAmount: orderItems.cartTotal,
					orderedBy: data.userId
				})
				
			 return newOrder.save().then((newOrder,error)=>{
					if (error){
						return false
					} else{
						return newOrder
					}
				})
			
	}

})
	return order
}

module.exports.removeFromCart=(data)=>{
	let isCartUpdated= Cart.findOne({orderedBy:data.userId}).then(cart=>{
		let test=cart.products.find(id=>id.productId==data.productId)
		if(test !==undefined){
		let index = cart.products.findIndex(i=>i.productId==data.productId)
		cart.products.splice(index,1)
		} else{
			return
		}

	
		let sum = 0
			for(let i=0;i<cart.products.length;i++){
					sum+=cart.products[i].subtotal
			}
			cart.cartTotal=sum
		// console.log(sum)
		cart.save()
		
	})

	let isUserUpdated=  User.findById(data.userId).then(user=>{
		let test=user.cart.find(id=>id.productId==data.productId)
		let index = user.cart.findIndex(i=>i.productId==data.productId)
		
		
			if(test==undefined){
				return false
			} else{
				
					user.cart.splice(index,1)
				
				return user.save().then((user,error)=>{
					if(error){
						return false
					} else {
						return true
					}
				})
			}		
	})
	return cart
	
}
