const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const orderSchema = new mongoose.Schema({

	products:[{
		productId: {
			type: String
		},
		name: {
			type: String
		},
		quantity: {
			type: Number,
			default: 1
		},
		subtotal: { 
			type: Number
		},
		isActive: {
			type: Boolean,
			default: true
		}
	}],
	
	totalAmount: {
		type: Number
	},
	purchasedOn:{
		type: Date,
		default: new Date()
	},
	orderedBy:{ 
		type: ObjectId, 
		ref: "User" 
	}

})
module.exports=mongoose.model('Order', orderSchema)
