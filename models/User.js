const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	cart: [
			{
				name: {
					type: String,
					//required: [true, "Product Name is required."]
				},
				description: {
					type: String,
					//required: [true, "Description is required."]
				},
				productId: {
					type: String,
					//required: [true, "Product ID is required."]
				},
				quantity: {
					type: Number,
					default: 1
				},
				price: {
					type: Number,
					//required: [true, "Price is required."]
				},
				imageURL: {
					type: String,
					//required: [true, "Image URL is required."]
				},
			}
		],
		order: [
				{
					userId: {
						type: String,
						required: [true, "User id is required."]
					},
					orderedOn: {
						type: Date,
						default: new Date()
					},
					status: {
						type: String,
						default: "Completed"
					},
					products: [
								{
									productId: {
										type: String,
										required: [true, "Product ID is required."]
									},
									quantity: {
										type: Number,
										required: [true, "Please input quantity."]
									},
									price: {
										type: Number,
										//required: [true, "Price is required."]
									}
								}
					],
					totalBill: {
						type: Number,
						required: [true, "Please compute total Price"]
					},
					/*shippingAddress: {
						firstName: {
							type: String,
							required: [true, "Please input first name."]
						},
						lastName: {
							type: String,
							required: [true, "Please input last name."]
						},
						mobileNo: {
							type: String,
							required: [true, "Please input mobile number."]
						},
						address: {
							type: String,
							required: [true, "Please input address."]
						}
					}*/
				}
		]
});


module.exports = mongoose.model("User", userSchema);
