const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	stocks: {
		type: Number,
		required: [true, "Stock is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	imageURL: {
		type: String
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	ordered: [
		{
			userId: {
				type: String,
				required: [true, "User id is required."]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("Product", productSchema);
