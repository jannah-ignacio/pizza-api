const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const cartSchema = new mongoose.Schema({
    
        products:[{
            productId: {
                type: String
            },
            name: {
                type: String
            },
            quantity: {
            type: Number,
            default: 1
            },
            subtotal: {
                type: Number
            }      
        }],

        cartTotal: {
            type: Number
        },
        orderedBy: { 
            type: ObjectId, 
            ref: "User" },
});

module.exports = mongoose.model("Cart", cartSchema);
