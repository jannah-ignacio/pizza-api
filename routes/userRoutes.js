const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Route for checking if email exists
router.post("/checkEmail", (req, res) => {
	
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for User Registration
router.post("/register", (req, res) => {
	
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Routes for User Authentication
router.post("/login", (req, res) => {
	
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
//The "auth.verify" will acts as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req, res) => {
		
	userController.getUser().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving one user's profile
router.get("/", auth.verify, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 
	
	userController.getOneUser(userId).then(resultFromController => res.send(resultFromController))
});

// Route for updating user's profile
router.put("/update", auth.verifyUser, (req, res) => {
	
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		updateUser: req.body
	}

	userController.userUpdate(data).then(resultFromController => res.send(resultFromController))
});	

// get all orders
router.get('/orders',auth.verifyAdmin,(req,res)=>{
	
	let data = auth.decode(req.headers.authorization)

	userController.getAllOrders(data).then(resultFromController=>res.send(resultFromController))
})

// get specific order
router.get('/myOrders',auth.verifyUser,(req,res)=>{

	let data = {userId : auth.decode(req.headers.authorization).id}

	userController.getOrder(data).then(resultFromController=>res.send(resultFromController))
}) 

// create order
router.post('/addToCart', auth.verifyUser, (req, res) => {
	
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	
	userController.createCart(data).then(resultFromController => res.send(resultFromController))
})

// delete all orders of specific user
router.delete('/myOrders/delete', auth.verifyUser, (req, res) => {

	let data = {userId:auth.decode(req.headers.authorization).id}

	userController.deleteAllOrder(data).then(resultFromController => res.send(resultFromController))
})

// delete specific product/order of user
router.delete('/myCart/deleteProduct', auth.verifyUser, (req, res) => {

	let data = {
		userId:auth.decode(req.headers.authorization).id,
		productId:req.body.productId
	}
	userController.deleteProduct(data).then(resultFromController => res.send(resultFromController))
})

router.get('/myCart', auth.verifyUser, (req, res) => {

	let data = {userId: auth.decode(req.headers.authorization).id}

		userController.getCart(data).then(resultFromController => res.send(resultFromController))
})

router.post('/checkout', auth.verifyUser, (req, res) => {
	
	let data = {userId: auth.decode(req.headers.authorization).id}

	userController.createOrder(data).then(resultFromController => res.send(resultFromController))
})

router.delete('/myCart/removeProduct', auth.verifyUser, (req, res) => {
	
	let data = {
		userId:auth.decode(req.headers.authorization).id,
		productId:req.body.productId
	}
		userController.removeFromCart(data).then(resultFromController => res.send(resultFromController))
})



module.exports=router

